# frozen_string_literal: true

RSpec.describe 'Sessions', type: :request do
  path '/api/sessions' do
    post 'Sign in' do
      tags 'Authentication'
      operationId 'signIn'

      consumes 'application/json'
      produces 'application/json'

      include_context 'with example response'

      parameter name: :params, in: :body, schema: {
        type: 'object',
        properties: {
          session: {
            type: 'object',
            properties: {
              email: { type: 'string', format: 'email' },
              password: { type: 'string' }
            }
          }
        },
        required: %w[email password]
      }

      response '201', 'User signed in' do
        let(:user) { create(:user) }
        let(:params) { { session: { email: user.email, password: user.password } } }

        run_test!

        schema(
          type: :object,
          properties: {
            data: { '$ref': '#/components/schemas/session' },
            included: {
              type: 'array',
              items: { '$ref': '#/components/schemas/user' }
            }
          }
        )
      end

      response '404', 'Invalid credentials' do
        let(:params) { { session: { email: 'some@mail.com', password: 'incorrect' } } }

        run_test!
      end
    end
  end
end
