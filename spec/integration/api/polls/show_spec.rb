# frozen_string_literal: true

RSpec.describe 'Polls', type: :request do
  path '/api/polls/{id}' do
    get 'show' do
      consumes 'application/json'
      produces 'application/json'

      include_context 'when authenticated'

      parameter name: :id, in: :path, type: :string

      let(:user) { create(:user) }
      let(:id) { create(:poll, user: user).id }

      response '200', 'Poll shown' do
        run_test!

        schema type: :object, properties: { data: { '$ref': '#/components/schemas/poll' } }
      end

      response '404', 'Poll not found' do
        let(:id) { 'missed' }

        run_test!
      end

      response '403', 'User not logged in or not owner of poll' do
        let(:Authorization) { nil }

        run_test!
      end
    end
  end
end
