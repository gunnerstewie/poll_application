# frozen_string_literal: true

RSpec.describe 'Polls', type: :request do
  path '/api/polls/{id}' do
    put 'Update' do
      consumes 'application/json'
      produces 'application/json'

      include_context 'when authenticated'

      parameter name: :id, in: :path, type: :string
      parameter name: :root, in: :body, schema: {
        type: 'object',
        properties: {
          poll: {
            type: 'object',
            properties: {
              title: { type: :string },
              description: { type: :string },
              startAt: { type: :string, format: 'date-time' },
              endAt: { type: :string, format: 'date-time' }
            }
          }
        }
      }

      let(:root) do
        {
          poll: {
            title: 'Somebody',
            description: 'was told me',
            startAt: 1.day.from_now,
            endAt: 1.week.from_now
          }
        }
      end
      let(:id) { create(:poll, user: user).id }

      response '204', 'Poll updated' do
        run_test!
      end

      response '404', 'Poll not found' do
        let(:id) { 'missing' }

        run_test!
      end

      response '422', 'given wrong params' do
        let(:root) do
          {
            poll: {
              title: ''
            }
          }
        end
        run_test!
      end

      response '403', 'user not logged in' do
        let(:Authorization) { nil }

        run_test!
      end
    end
  end
end
