# frozen_string_literal: true

RSpec.describe 'Polls', type: :request do
  path '/api/polls' do
    post 'Create Poll' do
      consumes 'application/json'
      produces 'application/json'

      include_context 'with example response'
      include_context 'when authenticated'

      parameter name: :params, in: :body, schema: {
        type: 'object',
        properties: {
          poll: {
            type: 'object',
            properties: {
              title: { type: 'string' },
              description: { type: 'string' },
              startAt: { type: 'string', format: 'date-time' },
              endAt: { type: 'string', format: 'date-time' }
            },
            required: %w[title startAt endAt]
          }
        },
        required: %w[poll]
      }

      response '201', 'Poll created' do
        let(:params) do
          { poll: { title: 'newamazingpoll', description: 'newamazingdescription', start_at: 1.day.from_now,
                    end_at: 1.week.from_now } }
        end
        run_test!

        schema type: :object, properties: { data: { '$ref': '#/components/schemas/poll' } }
      end

      response '422', 'given wrong params' do
        let(:params) { {} }

        run_test!
      end

      response '403', 'user not logged in' do
        let(:Authorization) { nil }
        let(:params) do
          { poll: { title: 'newamazingpoll', description: 'newamazingdescription', start_at: 1.day.from_now,
                    end_at: 1.week.from_now } }
        end

        run_test!
      end
    end
  end
end
