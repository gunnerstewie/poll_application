# frozen_string_literal: true

RSpec.describe 'Users', type: :request do
  path '/api/users' do
    post 'Sign up' do
      tags 'Users'
      operationId 'signUp'

      consumes 'application/json'
      produces 'application/json'

      include_context 'with example response'

      parameter name: :params, in: :body, schema: {
        type: 'object',
        properties: {
          user: {
            type: 'object',
            properties: {
              email: { type: 'string', format: 'email' },
              password: { type: 'string' },
              name: { type: 'string' }
            },
            required: %w[email password name]
          }
        },
        required: %w[user]
      }

      response '201', 'User signed up' do
        let(:params) { { user: { email: 'new@user.com', name: 'vasiliy', password: 'password' } } }

        run_test!

        schema type: :object, properties: { data: { '$ref': '#/components/schemas/user' } }
      end

      response '422', 'given wrong params: user not signed up' do
        let(:params) { {} }

        run_test!
      end
    end
  end
end
