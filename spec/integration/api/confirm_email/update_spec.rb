# frozen_string_literal: true

RSpec.describe 'Confirm email', type: :request do
  path '/api/confirm_email/{token}' do
    put 'Confirm email' do
      tags 'Confirm email'
      operationId 'confirmEmail'

      consumes 'application/json'
      produces 'application/json'

      parameter name: :token, in: :path, type: 'string'

      response '204', 'Confirmed' do
        let(:user) { create(:user, :with_unconfirmed_email) }
        let(:token) { user.confirmation_token }

        run_test!
      end

      response '404', 'Token not found' do
        let(:token) { 'unknown' }

        run_test!
      end
    end
  end
end
