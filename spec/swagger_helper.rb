# frozen_string_literal: true

require 'rails_helper'

module Swagger
  # Resource identifier object (https://jsonapi.org/format/#document-resource-identifier-objects)
  RESOURCE_IDENTIFIER_OBJECT = {
    type: 'object',
    properties: {
      id: { type: 'string' },
      type: { type: 'string' }
    }
  }.freeze

  # Relationship -- one member
  RELATIONSHIP_ONE = {
    type: 'object',
    properties: {
      data: RESOURCE_IDENTIFIER_OBJECT
    }
  }.freeze
end

RSpec.configure do |config|
  config.swagger_root = Rails.root.join('swagger').to_s

  config.swagger_docs = {
    'v1/swagger.yaml' => {
      openapi: '3.0.1',
      info: {
        title: 'API V1',
        version: 'v1'
      },
      paths: {},
      components: {
        securitySchemes: {
          bearer: {
            description: 'Key necessary to use API calls as authenticated user',
            type: :apiKey,
            name: 'Authorization',
            in: :header
          }
        },

        schemas: {
          user: {
            type: 'object',
            properties: {
              id: { type: 'string' },
              type: { type: 'string' },
              attributes: {
                type: 'object',
                properties: {
                  email: { type: 'string', format: 'email' },
                  name: { type: 'string' }
                },
                required: %w[email name]
              }
            },
            required: %w[id type attributes]
          },
          session: {
            type: 'object',
            properties: {
              id: { type: 'string' },
              type: { type: 'string' },
              attributes: {
                type: 'object',
                properties: {
                  token: { type: 'string' }
                },
                required: %w[token]
              },
              relationships: {
                type: 'object',
                properties: {
                  user: Swagger::RELATIONSHIP_ONE
                }
              }
            }
          },
          poll: {
            type: 'object',
            properties: {
              id: { type: 'string' },
              type: { type: 'string' },
              attributes: {
                type: 'object',
                properties: {
                  title: { type: 'string' },
                  description: { type: 'string' },
                  startAt: { type: 'string', format: 'date-time' },
                  endAt: { type: 'string', format: 'date-time' }
                },
                required: %w[title startAt endAt]
              },
              relationships: {
                type: 'object',
                properties: {
                  user: Swagger::RELATIONSHIP_ONE
                }
              }
            }
          }

        }
      }
    }
  }
  config.swagger_format = :yaml
end
JSON::Validator.class_variable_get('@@default_opts')[:strict] = true
