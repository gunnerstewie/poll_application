# frozen_string_literal: true

RSpec.configure do |config|
  config.before :each, :with_sendgrid_request do
    stub_request(:post, /api.sendgrid.com/).to_return(
      status: 200,
      body: {}.to_json
    )
  end
end
