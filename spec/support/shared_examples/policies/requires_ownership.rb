# frozen_string_literal: true

# rubocop:disable RSpec/SharedContext
RSpec.shared_examples 'requires ownership' do
  let(:context) { { user: user } }
  let(:record) { create(:poll) }

  failed 'when user not logged in' do
    let(:user) { nil }
  end

  failed 'when user is not owner' do
    let(:user) { build_stubbed(:user) }
  end

  succeed 'when user logged in and owner' do
    let(:user) { create(:user) }
    let(:record) { create(:poll, user: user) }
  end
end
# rubocop:enable RSpec/SharedContext
