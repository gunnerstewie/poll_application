# frozen_string_literal: true

RSpec.describe Users::CreateInput do
  subject { contract.(input) }

  let(:contract) { described_class.new }

  context 'with valid input' do
    let(:input) { { user: { email: 'example@gmail.com', name: 'Alexey', password: 'password' } } }

    it { is_expected.to be_success }
  end

  describe 'with invalid input' do
    context 'with invalid email' do
      subject(:errors) { contract.(user: { email: email }).errors[:user][:email] }

      let(:email) { 'qwerty' }

      it { is_expected.to include 'is in invalid format' }
    end

    context 'with invalid password' do
      subject(:errors) { contract.(user: { password: password }).errors[:user][:password] }

      let(:password) { '123' }

      it { is_expected.to include 'size cannot be less than 6' }
    end

    context 'with too big name' do
      subject(:errors) { contract.(user: { name: name }).errors[:user][:name] }

      let(:name) { 'a' * 42 }

      it { is_expected.to include 'size cannot be greater than 40' }
    end

    context 'with blank attribute' do
      subject(:errors) { contract.(user: { name: name }).errors[:user][:name] }

      let(:name) { '' }

      it { is_expected.to include 'must be filled' }
    end
  end
end
