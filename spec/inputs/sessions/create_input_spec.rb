# frozen_string_literal: true

RSpec.describe Sessions::CreateInput do
  subject { contract.(input) }

  let(:contract) { described_class.new }

  context 'with valid input' do
    let(:input) { { session: { email: 'example@gmail.com', password: 'password' } } }

    it { is_expected.to be_success }
  end

  describe 'with invalid input' do
    context 'with invalid email' do
      subject(:errors) { contract.(session: { email: email }).errors[:session][:email] }

      let(:email) { 'qwerty' }

      it { is_expected.to include 'is in invalid format' }
    end

    context 'with invalid password' do
      subject(:errors) { contract.(session: { password: password }).errors[:session][:password] }

      let(:password) { '123' }

      it { is_expected.to include 'size cannot be less than 6' }
    end

    context 'with blank attribute' do
      subject(:errors) { contract.(session: { email: email }).errors[:session][:email] }

      let(:email) { '' }

      it { is_expected.to include 'must be filled' }
    end
  end
end
