# frozen_string_literal: true

RSpec.describe Polls::CreateInput do
  subject(:create_input) { contract.(input) }

  let(:contract) { described_class.new }

  context 'with valid input' do
    let(:input) do
      { poll: { title: 'New Amazing Poll', start_at: 1.day.from_now.iso8601, end_at: 1.week.from_now.iso8601 } }
    end

    it { expect(create_input).to be_success }
  end

  describe 'with invalid input' do
    context 'with invalid title' do
      subject(:errors) { contract.(poll: { title: title }).errors[:poll][:title] }

      let(:title) { 'a' * 41 }

      it { is_expected.to include 'size cannot be greater than 40' }
    end

    context 'with invalid date format' do
      subject(:errors) { contract.(poll: { start_at: start_at }).errors[:poll][:start_at] }

      let(:start_at) { 'qwerty' }

      it { is_expected.to include 'must be a date time' }
    end

    context 'with empty attributes' do
      subject(:errors) { contract.(poll: { title: title }).errors[:poll][:title] }

      let(:title) { '' }

      it { is_expected.to include 'must be filled' }
    end

    context 'when start_at in past time' do
      subject(:errors) { contract.(poll: { start_at: start_at }).errors[:poll][:start_at] }

      let(:start_at) { 1.day.ago.iso8601 }

      it { is_expected.to include 'must be in the future' }
    end
  end
end
