# frozen_string_literal: true

RSpec.describe Authentication::WithEmailAndPassword do
  subject(:authenticate) { described_class.new }

  let(:user) { create(:user) }

  context 'with successful authentication' do
    it 'return user for valid credentials' do
      expect(authenticate.(email: user.email, password: user.password)).to eq user
    end
  end

  context 'with failure authentication' do
    it 'raise an error if user not found' do
      expect do
        authenticate.(email: 'wrong-user@mail.com', password: '1234567')
      end.to raise_error ActiveRecord::RecordNotFound
    end

    it 'rise an error if wrong user email' do
      expect do
        authenticate.(email: 'wrong@mail.com', password: user.password)
      end.to raise_error ActiveRecord::RecordNotFound
    end

    it 'raise an erros if wrong user password' do
      expect { authenticate.(email: user.email, password: '654321') }.to raise_error ActiveRecord::RecordNotFound
    end
  end
end
