# frozen_string_literal: true

RSpec.describe Sendgrid::SendDynamicEmail do
  let(:service) { described_class.new }
  let(:sender) { Faker::Internet.email }
  let(:params) do
    {
      to: Faker::Internet.email,
      template_id: 'TEMPLATE_ID',
      template_data: described_class::DEFAULT_TEMPLATE_DATA.merge('PARAM' => 'VALUE')
    }
  end
  let!(:stubbed_request) do
    stub_request(:post, /api.sendgrid.com/).with(
      body: {
        from: { email: sender },
        template_id: params[:template_id],
        personalizations: [
          {
            to: [{ email: params[:to] }],
            dynamic_template_data: params[:template_data]
          }
        ]
      }
    )
  end

  before { stub_const("#{described_class}::FROM_EMAIL", sender) }

  it 'calls sendgrid api with right params' do
    service.(**params)
    expect(stubbed_request).to have_been_requested
  end
end
