# frozen_string_literal: true

RSpec.describe Users::CreateConfirmationToken do
  subject(:new_token) { service.(user) }

  let(:service) { described_class.new }
  let(:user) { create(:user) }

  it 'creates new token' do
    new_token

    expect(user.confirmation_token).not_to be_nil
    expect(user.confirmed_at).to be_nil
  end
end
