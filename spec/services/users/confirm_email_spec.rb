# frozen_string_literal: true

RSpec.describe Users::ConfirmEmail do
  let(:service) { described_class.new }

  let(:user) { create(:user, :with_unconfirmed_email) }

  it 'updates confirmation related fields', :freeze_time do
    service.(user)
    expect(user.confirmation_token).to eq nil
    expect(user.confirmed_at).to eq Time.current
  end
end
