# frozen_string_literal: true

RSpec.describe Users::Create do
  subject(:user) { described_class.new.(input) }

  context 'when successful creating user' do
    let(:email) { 'example@gmail.com' }
    let(:name) { 'Alexey' }
    let(:password) { 'password' }
    let(:input) { { email: email, name: name, password: password } }

    it 'create valid user' do
      expect(user.email).to eq email
      expect(user.name).to eq name
    end
  end

  context 'when duplicate email given' do
    let(:existing) { create(:user, email: 'example@gmail.com', name: 'Alexey', password: 'password') }

    let(:input) { { email: existing.email, name: existing.name, password: '123456' } }

    it 'raise NotUnique error' do
      expect { user }.to raise_error ActiveRecord::RecordNotUnique
    end
  end
end
