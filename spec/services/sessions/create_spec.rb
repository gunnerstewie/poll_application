# frozen_string_literal: true

RSpec.describe Sessions::Create do
  subject(:create_session) { described_class.new.(user) }

  context 'when session created successfully' do
    let(:user) { create(:user) }

    it 'create valid session' do
      expect { create_session }.to change(user.sessions, :count).by(1)
    end
  end
end
