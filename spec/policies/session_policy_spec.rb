# frozen_string_literal: true

RSpec.describe SessionPolicy do
  let(:context) { { user: user } }
  let(:user) { nil }

  describe_rule :create? do
    failed 'when user login' do
      let(:user) { build_stubbed(:user) }
    end
    succeed 'when user is not logged in'
  end
end
