# frozen_string_literal: true

RSpec.describe UserPolicy do
  let(:user) { nil }
  let(:context) { { user: user } }

  describe_rule :create? do
    succeed 'always'
  end

  describe_rule :confirm_email? do
    succeed 'always'
  end
end
