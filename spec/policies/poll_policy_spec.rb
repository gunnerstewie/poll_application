# frozen_string_literal: true

RSpec.describe PollPolicy do
  let(:context) { { user: user } }

  describe_rule :create? do
    include_examples 'requires login'
  end

  describe_rule :update? do
    include_examples 'requires ownership'
  end

  describe_rule :destroy? do
    include_examples 'requires ownership'
  end

  describe_rule :show? do
    include_examples 'requires ownership'
  end
end
