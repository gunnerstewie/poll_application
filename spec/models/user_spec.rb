# frozen_string_literal: true

require 'rails_helper'

RSpec.describe User, type: :model do
  it { is_expected.to have_secure_password }
  it { is_expected.to have_many(:sessions) }

  describe '.email_confirmed?' do
    it { expect(build(:user, confirmed_at: nil)).not_to be_email_confirmed }
    it { expect(build(:user, confirmed_at: Time.current)).to be_email_confirmed }
  end
end
