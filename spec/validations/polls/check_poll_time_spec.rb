# frozen_string_literal: true

RSpec.describe Polls::CheckPollTime do
  subject(:check) { service.(poll) }

  let(:service) { described_class.new }

  context 'when poll time is correct' do
    let(:poll) { create(:poll) }

    it { expect { check }.not_to raise_error }
  end

  context 'when poll time is incorrect' do
    let(:poll) { create(:poll, start_at: 1.day.from_now, end_at: 1.week.ago) }

    it { expect { check }.to raise_validation_error }
  end

  private

  def raise_validation_error
    raise_error RequestError, I18n.t('errors.polls.invalid_input')
  end
end
