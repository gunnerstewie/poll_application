# frozen_string_literal: true

RSpec.describe Polls::CreateAction do
  subject(:create_action) do
    service.(poll_params)
  end

  let(:service) { described_class.new }

  let(:user) { create(:user) }
  let(:poll_params) do
    { title: 'newamazingpoll', description: 'newamazingdescription', start_at: 1.day.from_now,
      end_at: 1.week.from_now, user: user }
  end

  context 'when poll created' do
    it 'successful creation' do
      poll = create_action
      expect(poll).to be_a(Poll)
      expect(poll).to be_persisted
    end
  end

  context 'when wrong params given' do
    let(:poll_params) do
      { title: 'newamazingpoll', description: 'newamazingdescription', start_at: 1.day.from_now,
        end_at: 1.week.ago, user: user }
    end

    it ' fail validation andraise exception' do
      expect { create_action }.to raise_error RequestError, I18n.t('errors.polls.invalid_input')
    end
  end
end
