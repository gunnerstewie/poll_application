# frozen_string_literal: true

RSpec.describe Polls::DestroyAction do
  subject(:destroy_action) { service.(poll) }

  let(:service) { described_class.new }
  let(:poll) { create(:poll) }

  context 'when action called successfully' do
    it 'call action' do
      destroy_action
      expect(poll).to be_destroyed
    end
  end
end
