# frozen_string_literal: true

RSpec.describe Polls::UpdateAction do
  subject(:update_action) do
    service.(poll, poll_params)
  end

  let(:service) { described_class.new }

  let(:poll_params) { { title: 'newupdatedpoll' } }
  let(:poll) { create(:poll) }

  context 'when poll updated successfully' do
    it 'update poll' do
      update_action
      expect(poll.title).to eq 'newupdatedpoll'
    end
  end

  context 'when wrong params given' do
    let(:poll_params) { { start_at: 1.day.from_now, end_at: 1.week.ago } }

    it 'raise exception' do
      expect { update_action }.to raise_error RequestError, I18n.t('errors.polls.invalid_input')
    end
  end
end
