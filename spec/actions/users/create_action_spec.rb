# frozen_string_literal: true

RSpec.describe Users::CreateAction do
  subject(:create_action) do
    service.(create_params)
  end

  let(:service) do
    described_class.new(
      create: create_user,
      create_confirmation_token: create_confirmation_token,
      send_confirmation_request: send_confirmation_request
    )
  end

  let(:create_user) { instance_double(Users::Create, call: user) }
  let(:create_confirmation_token) { instance_double(Users::CreateConfirmationToken, call: nil) }
  let(:send_confirmation_request) { instance_double(Users::SendConfirmationRequestWorker, call: nil) }

  context 'with successful action call' do
    let(:user) { build_stubbed(:user) }
    let(:create_params) { {} }

    it 'create user successfuly' do
      create_action

      expect(create_user).to have_received(:call).with(create_params)
      expect(create_confirmation_token).to have_received(:call).with(user)
      expect(send_confirmation_request).to have_received(:call).with(user.id)
    end
  end

  context 'when raise not unique exception' do
    let(:user) { nil }
    let(:create_params) { { email: 'example@gmail.com' } }

    before { allow(create_user).to receive(:call).and_raise(ActiveRecord::RecordNotUnique) }

    it 'raise exception' do
      expect { create_action }.to raise_error RequestError, I18n.t('errors.users.email_taken')
    end
  end
end
