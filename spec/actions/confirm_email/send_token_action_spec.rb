# frozen_string_literal: true

RSpec.describe ConfirmEmail::SendTokenAction do
  subject(:send_action) do
    service.(user)
  end

  let(:service) do
    described_class.new(
      create_confirmation_token: create_token,
      send_confirmation_request: send_request
    )
  end

  context 'when action called successfully' do
    let(:user) { build_stubbed(:user) }
    let(:create_token) { instance_double(Users::CreateConfirmationToken, call: nil) }
    let(:send_request) { instance_double(Users::SendConfirmationRequestWorker, call: nil) }

    it 'calls action successfully' do
      send_action
      expect(create_token).to have_received(:call).with(user)
      expect(send_request).to have_received(:call).with(user.id)
    end
  end
end
