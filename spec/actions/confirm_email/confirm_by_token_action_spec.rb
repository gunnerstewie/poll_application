# frozen_string_literal: true

RSpec.describe ConfirmEmail::ConfirmByTokenAction do
  subject(:confirm_action) do
    service.(token)
  end

  let(:service) do
    described_class.new(
      confirm_email: confirm_email
    )
  end

  context 'when action called successfully' do
    let(:user) { create(:user, confirmation_token: 'token') }
    let(:confirm_email) { instance_double(Users::ConfirmEmail, call: user) }
    let(:token) { user.confirmation_token }

    it 'called action successfully' do
      confirm_action
      expect(confirm_email).to have_received(:call).with(user)
    end
  end

  context 'when raise exception' do
    let(:user) { build_stubbed(:user) }
    let(:confirm_email) { instance_double(Users::ConfirmEmail, call: nil) }
    let(:token) { 'token' }

    before { allow(confirm_email).to receive(:call).and_raise(ActiveRecord::RecordNotFound) }

    it 'raise exception' do
      expect { confirm_action }.to raise_error ActiveRecord::RecordNotFound
    end
  end
end
