# frozen_string_literal: true

RSpec.describe Auth::SignInAction do
  subject(:sign_in_action) do
    service.(email: email, password: password)
  end

  let(:service) do
    described_class.new(
      create_session: create_session
    )
  end

  context 'when action called successfully' do
    let(:user) { create(:user, email: email, password: password) }
    let(:email) { 'example@gmail.com' }
    let(:password) { 'password' }
    let(:create_session) { instance_double(Sessions::Create, call: user) }

    it 'called action successfully' do
      sign_in_action
      expect(create_session).to have_received(:call).with(user)
    end
  end

  context 'when invalid params' do
    let(:user) { nil }
    let(:email) { 'example@gmail.com' }
    let(:password) { 'password' }
    let(:create_session) { instance_double(Sessions::Create, call: nil) }

    it 'raise exception' do
      expect do
        sign_in_action
      end.to raise_error ActiveRecord::RecordNotFound, I18n.t('errors.sessions.invalid_credentials')
    end
  end
end
