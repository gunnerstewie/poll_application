# frozen_string_literal: true

RSpec.describe 'PUT /api/confirm_email/:token', type: :request do
  let(:path) { "/api/confirm_email/#{token}" }

  let(:user) { create(:user, :with_unconfirmed_email) }
  let(:token) { user.confirmation_token }

  context 'when token is valid', :freeze_time do
    it 'confirms user' do
      put path

      expect(response).to have_http_status :no_content

      user.reload
      expect(user.confirmation_token).to eq nil
      expect(user.confirmed_at).to eq Time.current
    end
  end

  context 'when token is invalid' do
    let(:token) { 'TOKEN' }

    it 'returns error' do
      put path

      expect(response).to have_http_status :not_found
      expect(body).to have_error("Couldn't find User").with_status(:not_found).with_code(ErrorCodes::NOT_FOUND)
    end
  end
end
