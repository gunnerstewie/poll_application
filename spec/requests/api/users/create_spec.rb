# frozen_string_literal: true

RSpec.describe 'POST api/users', type: :request do
  let(:path) { '/api/users' }

  let(:email) { 'test23123@mail.com' }
  let(:params) { { user: user_params } }

  let(:user_params) do
    {
      email: email,
      password: '123456',
      name: 'Andrey'
    }
  end

  context 'when params are valid' do
    context 'with full info' do
      it 'creates new user account' do
        post path, params: params

        user = User.last
        expect(response).to have_http_status :created
        expect(body[:data]).to eq(
          id: user.id.to_s,
          type: 'user',
          attributes: {
            email: email,
            name: 'Andrey'
          }
        )
      end
    end
  end

  context 'when params are invalid' do
    let(:params) { {} }

    it 'returns errors' do
      post path, params: params

      expect(response).to have_http_status :unprocessable_entity
    end
  end

  context 'when email not unique' do
    let(:user) { create(:user) }
    let(:params) { { user: { email: user.email, password: 'some_password', name: 'Alexey' } } }

    it 'return error' do
      post path, params: params

      expect(response).to have_http_status :unprocessable_entity
      expect(body).to have_error(I18n.t('errors.users.email_taken'))
        .with_status(:unprocessable_entity)
        .on_field('user/email')
        .with_code(ErrorCodes::INVALID)
    end
  end
end
