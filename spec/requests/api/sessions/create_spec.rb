# frozen_string_literal: true

RSpec.describe 'POST api/sessions', type: :request do
  let(:path) { '/api/sessions' }

  context 'with valid email and password' do
    let(:user) { create(:user) }
    let(:params) { { session: { email: user.email, password: user.password } } }

    it 'create session and return it' do
      post path, params: params

      session = Session.last

      expect(response).to have_http_status :created

      expect(body[:data]).to eq(
        id: session.id.to_s,
        type: 'session',
        attributes: {
          token: session.id.to_s
        },
        relationships: {
          user: {
            data: {
              id: session.user.id.to_s,
              type: 'user'
            }
          }
        }
      )
    end
  end

  context 'with invalid email' do
    let(:params) { { session: { email: 'invalid@mail.com', password: 'user.password' } } }

    it 'return invalid credital error' do
      post path, params: params

      expect(response).to have_http_status(:not_found)
      expect(body).to have_error(I18n.t('errors.sessions.invalid_credentials'))
        .with_status(:not_found)
        .with_code(ErrorCodes::NOT_FOUND)
    end
  end

  context 'with invalid password' do
    let(:user) { create(:user) }
    let(:params) { { session: { email: user.email, password: '98765432' } } }

    it 'return invalid credital error' do
      post path, params: params
      expect(response).to have_http_status(:not_found)
      expect(body).to have_error(I18n.t('errors.sessions.invalid_credentials'))
        .with_status(:not_found)
        .with_code(ErrorCodes::NOT_FOUND)
    end
  end

  context 'with already logged in' do
    let(:user) { create(:user) }

    it 'return unauthorize error' do
      post path, headers: headers_for_user(user)

      expect(response).to have_http_status(:forbidden)
      expect(body).to have_error('You are logged in.')
        .with_status(:forbidden)
        .with_code(ErrorCodes::UNAUTHORIZED)
    end
  end
end
