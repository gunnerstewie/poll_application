# frozen_string_literal: true

RSpec.describe 'GET /api/polls/:id', type: :request do
  let(:path) { "/api/polls/#{poll.id}" }

  let(:user) { create(:user) }

  context 'when user logged in' do
    let(:poll) do
      create(:poll, title: 'New Amazing Title', description: 'newamazing', start_at: 1.day.from_now,
     end_at: 1.week.from_now, user: user)
    end

    context 'when successfully return poll', :freeze_time do
      it 'return  poll' do
        get path, headers: headers_for_user(user)

        expect(response).to have_http_status :success
        expect(body[:data]).to eq(
          id: poll.id.to_s,
          type: 'poll',
          attributes: {
            title: 'New Amazing Title',
            description: 'newamazing',
            startAt: (1.day.from_now).strftime('%Y-%m-%dT%H:%M:%S.%LZ'),
            endAt: (1.week.from_now).strftime('%Y-%m-%dT%H:%M:%S.%LZ')
          },
          relationships: {
            user: {
              data: {
                id: poll.user.id.to_s,
                type: 'user'
              }
            }
          }
        )
      end
    end
  end

  context 'when dont show poll because user not logged in' do
    let(:poll) { create(:poll) }

    it 'with no user it raise exception' do
      get path
      expect(response).to have_http_status :forbidden
    end
  end

  context 'when user is not owner of poll' do
    let(:poll) { create(:poll) }

    it 'because user not owner it raise exception' do
      get path, headers: headers_for_user(user)

      expect(response).to have_http_status :forbidden
    end
  end
end
