# frozen_string_literal: true

RSpec.describe 'DELETE /api/polls/:id', type: :request do
  let(:path) { "/api/polls/#{poll.id}" }

  let(:user) { create(:user) }
  let(:poll) { create(:poll, user: user) }

  context 'when user logged in' do
    context 'when poll deleted successfully' do
      it 'delete poll' do
        delete path, headers: headers_for_user(user)

        expect(response).to have_http_status :no_content
        expect(body).to eq({})
      end
    end
  end

  context 'when user not logged in' do
    it 'raise not_authorized status' do
      delete path

      expect(response).to have_http_status :forbidden
    end
  end

  context 'when user is not owner of poll' do
    let(:poll) { create(:poll) }

    it 'because user not owner it raise exception' do
      delete path, headers: headers_for_user(user)

      expect(response).to have_http_status :forbidden
    end
  end
end
