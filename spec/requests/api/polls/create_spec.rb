# frozen_string_literal: true

RSpec.describe 'POST /api/polls', type: :request do
  let(:path) { '/api/polls' }

  context 'with valid attributes and existing user', :freeze_time do
    let(:user) { create(:user) }
    let(:params) do
      { poll: { title: 'New Amazing Title', description: 'newamazing', start_at: 1.day.from_now,
                end_at: 1.week.from_now } }
    end

    it 'create new poll' do
      post path, params: params, headers: headers_for_user(user)

      poll = Poll.last

      expect(response).to have_http_status :created
      expect(body[:data]).to eq(
        id: poll.id.to_s,
        type: 'poll',
        attributes: {
          title: 'New Amazing Title',
          description: 'newamazing',
          startAt: (1.day.from_now).strftime('%Y-%m-%dT%H:%M:%S.%LZ'),
          endAt: (1.week.from_now).strftime('%Y-%m-%dT%H:%M:%S.%LZ')
        },
        relationships: {
          user: {
            data: {
              id: poll.user.id.to_s,
              type: 'user'
            }
          }
        }
      )
    end
  end

  context 'with invalid params' do
    let(:user) { create(:user) }
    let(:params) { {} }

    it ' raise 422 bad request' do
      post path, params: params, headers: headers_for_user(user)

      expect(response).to have_http_status :unprocessable_entity
    end
  end

  context 'with not logged in user' do
    let(:params) do
      { poll: { title: 'New Amazing Title', start_at: 1.day.from_now, end_at: 1.week.from_now } }
    end

    it 'raise 403 forbidden' do
      post path, params: params

      expect(response).to have_http_status :forbidden
    end
  end
end
