# frozen_string_literal: true

RSpec.describe 'PUT api/polls/:id', type: :request do
  let(:path) { "/api/polls/#{poll.id}" }

  let(:user) { create(:user) }
  let(:poll) { create(:poll, user: user) }

  context 'when successful updating poll', :freeze_time do
    let(:params) { { poll: { title: 'Some changed Title' } } }

    it 'update poll' do
      put path, params: params, headers: headers_for_user(user)

      expect(response).to have_http_status :no_content
      poll.reload
      expect(poll.title).to eq('Some changed Title')
    end
  end

  context 'when updating poll is failure' do
    let(:params) { { poll: { title: '' } } }

    it 'with invalid params' do
      put path, params: params, headers: headers_for_user(user)

      expect(response).to have_http_status :unprocessable_entity
    end
  end

  context 'when user not logged in' do
    let(:params) { { poll: { title: 'Some changed Title' } } }

    it 'with no user' do
      put path, params: params

      expect(response).to have_http_status :forbidden
    end
  end

  context 'when user not owner of poll' do
    let(:poll) { create(:poll) }
    let(:params) { { poll: { title: 'Some changed Title' } } }

    it 'because user not owner it raise exception' do
      put path, params: params, headers: headers_for_user(user)

      expect(response).to have_http_status :forbidden
    end
  end
end
