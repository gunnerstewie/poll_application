# frozen_string_literal: true

FactoryBot.define do
  factory :poll do
    user
    sequence(:title) { |i| "New Amazing Poll#{i}" }
    sequence(:description) { |i| "New Amazing Description#{i}" }
    start_at { 1.day.from_now }
    end_at { 1.week.from_now }
  end
end
