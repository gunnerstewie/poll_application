# frozen_string_literal: true

FactoryBot.define do
  factory :user do
    sequence(:email) { |i| "user-#{i}@example.com" }
    password { '123456' }
    sequence(:name) { |i| "#{i}Vasiliy" }
    confirmed_at { Time.current }

    trait :with_unconfirmed_email do
      confirmed_at { nil }
      confirmation_token { 'token' }
    end
  end
end
