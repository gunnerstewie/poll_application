# frozen_string_literal: true

RSpec.describe Users::SendConfirmationRequestWorker do
  subject(:send_request) { worker.perform(user.id) }

  let(:worker) { described_class.new(send_email: send_email) }
  let(:user) { create(:user) }

  let(:send_email) { instance_double(Users::ConfirmationRequestMailer, call: nil) }

  it 'delivers email and sets the sent_at time' do
    send_request
    user.reload

    expect(send_email).to have_received(:call).with(user)
  end

  it 'does not deliver email to missing users' do
    worker.perform(0)

    expect(send_email).not_to have_received(:call)
  end
end
