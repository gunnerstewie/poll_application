# frozen_string_literal: true

RSpec.describe Users::ConfirmationRequestMailer do
  let(:service) do
    described_class.new(send_email: send_email)
  end

  let(:send_email) { instance_double(Sendgrid::SendDynamicEmail, call: nil) }
  let(:user) { build(:user, confirmation_token: token) }
  let(:token) { 'TOKEN' }

  it 'calls send email service with right params' do
    service.(user)
    expect(send_email).to have_received(:call).with(
      to: user.email,
      template_id: 'confirmation-request',
      template_data: { token: token }
    )
  end
end
