# frozen_string_literal: true

module Sendgrid
  TEMPLATES = YAML.load_file('config/sendgrid_templates.yml').fetch(Rails.env).freeze
  SERVER_URL = ENV.fetch('SERVER_URL')
end
