# frozen_string_literal: true

module AutoInjectWorkers
  extend Dry::Container::Mixin

  def self.extended(container)
    container.instance_exec do
      namespace :workers do
        namespace :users do
          register(:send_confirmation_request) { Users::SendConfirmationRequestWorker.new }
        end
      end
    end
  end
end
