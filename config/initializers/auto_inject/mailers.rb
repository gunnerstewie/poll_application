# frozen_string_literal: true

module AutoInjectMailers
  extend Dry::Container::Mixin

  def self.extended(container)
    container.instance_exec do
      namespace :mailers do
        namespace :users do
          register(:confirmation_request) { Users::ConfirmationRequestMailer.new }
        end
      end
    end
  end
end
