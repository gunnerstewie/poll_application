# frozen_string_literal: true

module AutoInjectActions
  extend Dry::Container::Mixin

  def self.extended(container)
    container.instance_exec do
      namespace :actions do
        namespace :auth do
          register(:sign_in) { Auth::SignInAction.new }
        end

        namespace :confirm_email do
          register(:confirm_by_token) { ConfirmEmail::ConfirmByTokenAction.new }
          register(:send_token) { ConfirmEmail::SendTokenAction.new }
        end

        namespace :polls do
          register(:create) { Polls::CreateAction.new }
          register(:update) { Polls::UpdateAction.new }
          register(:destroy) { Polls::DestroyAction.new }
        end

        namespace :users do
          register(:create) { Users::CreateAction.new }
        end
      end
    end
  end
end
