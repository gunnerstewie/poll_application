# frozen_string_literal: true

# Allows to automatically inject dependencies to object constructors from a
# configured container.
# http://dry-rb.org/gems/dry-auto_inject/
module AutoInjectServices
  extend Dry::Container::Mixin

  def self.extended(container)
    container.instance_exec do
      namespace :services do
        register(:validate_input) { ValidateInput.new }

        namespace :authentication do
          register(:with_email_and_password) { Authentication::WithEmailAndPassword.new }
        end

        namespace :sendgrid do
          register(:send_dynamic_email) { Sendgrid::SendDynamicEmail.new }
        end

        namespace :sessions do
          register(:create) { Sessions::Create.new }
        end

        namespace :users do
          register(:confirm_email) { Users::ConfirmEmail.new }
          register(:create) { Users::Create.new }
          register(:create_confirmation_token) { Users::CreateConfirmationToken.new }
        end
      end
    end
  end
end
