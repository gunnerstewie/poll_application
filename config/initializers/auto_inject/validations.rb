# frozen_string_literal: true

module AutoInjectValidations
  extend Dry::Container::Mixin

  def self.extended(container)
    container.instance_exec do
      namespace :validations do
        namespace :polls do
          register(:check_poll_time) { Polls::CheckPollTime.new }
        end
      end
    end
  end
end
