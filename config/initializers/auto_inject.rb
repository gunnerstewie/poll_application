# frozen_string_literal: true

require_relative 'auto_inject/actions'
require_relative 'auto_inject/mailers'
require_relative 'auto_inject/services'
require_relative 'auto_inject/workers'
require_relative 'auto_inject/inputs'
require_relative 'auto_inject/validations'

class DryContainer
  extend Dry::Container::Mixin

  extend AutoInjectActions
  extend AutoInjectMailers
  extend AutoInjectServices
  extend AutoInjectWorkers
  extend AutoInjectInputs
  extend AutoInjectValidations
end

DI = Dry::AutoInject(DryContainer)
