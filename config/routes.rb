# frozen_string_literal: true

Rails.application.routes.draw do
  namespace :api, defaults: { format: 'json' } do
    if ENV.fetch('DOCS_ENABLED', '0') == '1'
      mount Rswag::Ui::Engine => '/docs'
      mount Rswag::Api::Engine => '/docs'
    end
    resources :confirm_email, only: %i[update]
    resources :polls, only: %i[create update destroy show]
    resources :sessions, only: %i[create]
    resources :users, only: %i[create]
  end
end
