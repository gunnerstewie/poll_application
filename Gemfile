# frozen_string_literal: true

source 'https://rubygems.org'
git_source(:github) { |repo| "https://github.com/#{repo}.git" }

ruby '2.7.3'

# Bundle edge Rails instead: gem 'rails', github: 'rails/rails', branch: 'main'
gem 'action_policy', '~> 0.5.7'
gem 'bcrypt', '~> 3.1.7'
gem 'dotenv-rails', '~> 2.7.6'
gem 'dry-auto_inject', '~> 0.6'
gem 'dry-validation', '~> 1.6.0'
gem 'hiredis', '~> 0.6.3'
gem 'jsonapi-serializer', '~> 2.2'
gem 'oj', '~> 3.12.0'
gem 'pg', '~> 1.1'
gem 'puma', '~> 5.0'
gem 'rails', '~> 6.1.4'
gem 'responders', '~> 3.0.1'
gem 'rswag', '~> 2.4.0'
gem 'sendgrid-ruby', '~> 6.4.0'
gem 'sidekiq', '~> 6.1', '>= 6.1.3'

# Reduces boot times through caching; required in config/boot.rb
gem 'bootsnap', '>= 1.4.4', require: false

group :development, :test do
  # Call 'byebug' anywhere in the code to stop execution and get a debugger console
  gem 'byebug', platforms: %i[mri mingw x64_mingw]
  gem 'rspec-rails', '~> 4.0.1'
  gem 'rswag-specs', '~> 2.4.0'
  gem 'simplecov', '~> 0.21.2', require: false
end

group :development do
  gem 'listen', '~> 3.3'
  gem 'reek', '~> 6.0.4'
  gem 'rubocop', '~> 1.18.3', require: false
  gem 'rubocop-performance', '~> 1.5', '>= 1.5.2', require: false
  gem 'rubocop-rails', '~> 2.2', require: false
  gem 'rubocop-rspec', '~> 2.0', require: false
end

group :test do
  gem 'database_cleaner-active_record', '~> 2.0.1'
  gem 'factory_bot_rails', '~> 6.2.0'
  gem 'faker', '~> 2.18.0'
  gem 'shoulda-matchers', '~> 4.5.1'
  gem 'webmock', '~> 3.13.0'
end
# Windows does not include zoneinfo files, so bundle the tzinfo-data gem
gem 'tzinfo-data', platforms: %i[mingw mswin x64_mingw jruby]
