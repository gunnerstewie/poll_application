# frozen_string_literal: true

class PollSerializer < ApplicationSerializer
  set_type :poll

  belongs_to :user
  attributes :description
  attributes :title
  attributes :start_at
  attributes :end_at
end
