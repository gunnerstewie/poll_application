# frozen_string_literal: true

class ConfirmEmail::SendTokenAction
  include DI[
    'services.users.create_confirmation_token',
    'workers.users.send_confirmation_request'
  ]

  def call(user)
    create_confirmation_token.(user)
    send_confirmation_request.(user.id)
  end
end
