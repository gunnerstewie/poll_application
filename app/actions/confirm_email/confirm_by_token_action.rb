# frozen_string_literal: true

class ConfirmEmail::ConfirmByTokenAction
  include DI[
    confirm_email: 'services.users.confirm_email'
  ]

  def call(token)
    user = User.find_by!(confirmation_token: token)

    confirm_email.(user)

    user
  end
end
