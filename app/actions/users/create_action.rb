# frozen_string_literal: true

class Users::CreateAction
  include DI[
    'services.users.create',
    'services.users.create_confirmation_token',
    'workers.users.send_confirmation_request'
  ]

  def call(create_params)
    create_user(create_params).tap do |user|
      send_confirmation_request.(user.id)
    end
  rescue ActiveRecord::RecordNotUnique
    raise RequestError.new(I18n.t('errors.users.email_taken'), field: 'user/email')
  end

  def create_user(create_params)
    ApplicationRecord.transaction do
      user = create.(create_params)
      create_confirmation_token.(user)
      user
    end
  end
end
