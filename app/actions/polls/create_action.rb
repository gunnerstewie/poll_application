# frozen_string_literal: true

class Polls::CreateAction
  include DI[
    'validations.polls.check_poll_time'
  ]

  def call(poll_params)
    poll = Poll.new(poll_params)
    validate(poll)
    poll.save!

    poll
  end

  def validate(poll)
    check_poll_time.(poll)
  end
end
