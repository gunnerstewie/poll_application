# frozen_string_literal: true

class Polls::UpdateAction
  include DI[
    'validations.polls.check_poll_time'
  ]

  # :reek:FeatureEnvy
  def call(poll, poll_params)
    poll.attributes = poll_params
    validate(poll)
    poll.save!
  end

  def validate(poll)
    check_poll_time.(poll)
  end
end
