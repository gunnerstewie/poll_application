# frozen_string_literal: true

class Polls::DestroyAction
  def call(poll)
    poll.destroy!
  end
end
