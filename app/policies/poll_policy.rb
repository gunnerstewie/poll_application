# frozen_string_literal: true

class PollPolicy < ApplicationPolicy
  def create?
    logged_in?
  end

  def update?
    owner?
  end

  def destroy?
    owner?
  end

  def show?
    owner?
  end
end
