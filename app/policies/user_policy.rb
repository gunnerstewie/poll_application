# frozen_string_literal: true

class UserPolicy < ApplicationPolicy
  def create?
    true
  end

  def confirm_email?
    true
  end
end
