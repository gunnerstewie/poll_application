# frozen_string_literal: true

class ApplicationPolicy < ActionPolicy::Base
  authorize :user, optional: true

  def logged_in?
    deny!(:not_logged_in) unless user
    true
  end

  def not_logged_in?
    deny!(:logged_in) if user
    true
  end

  def owner?
    deny! unless logged_in? && record.user == user
    true
  end
end
