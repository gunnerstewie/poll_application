# frozen_string_literal: true

class Authentication::WithEmailAndPassword
  def call(email:, password:)
    user = authenticate(email, password)

    raise ActiveRecord::RecordNotFound, I18n.t('errors.sessions.invalid_credentials') unless user

    user
  end

  def authenticate(email, password)
    user = User.find_by('LOWER(email) = ?', email.downcase)

    return if user&.password_digest.blank?

    user.authenticate(password)
  end
end
