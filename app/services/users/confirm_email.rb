# frozen_string_literal: true

class Users::ConfirmEmail
  def call(user)
    user.update!(
      confirmation_token: nil,
      confirmed_at: Time.current
    )
  end
end
