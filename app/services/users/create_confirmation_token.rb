# frozen_string_literal: true

class Users::CreateConfirmationToken
  def call(user)
    user.update!(
      confirmation_token: SecureRandom.uuid,
      confirmed_at: nil
    )
  end
end
