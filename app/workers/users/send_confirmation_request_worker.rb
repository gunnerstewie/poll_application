# frozen_string_literal: true

class Users::SendConfirmationRequestWorker
  include DryWorker
  include DI[
    send_email: 'mailers.users.confirmation_request'
  ]

  def perform(user_id)
    user = User.find(user_id)

    send_email.(user)

    user
  rescue ActiveRecord::RecordNotFound
    # Record was deleted; skip
  end
end
