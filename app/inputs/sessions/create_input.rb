# frozen_string_literal: true

class Sessions::CreateInput < ApplicationContract
  json do
    required(:session).hash do
      required(:email).filled(Types::Email)
      required(:password).filled(Types::StrippedString, min_size?: User::MIN_PASSWORD_LENGTH)
    end
  end
end
