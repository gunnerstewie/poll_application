# frozen_string_literal: true

class Users::CreateInput < ApplicationContract
  json do
    required(:user).hash do
      required(:email).filled(Types::Email)
      required(:password).filled(Types::StrippedString, min_size?: User::MIN_PASSWORD_LENGTH)
      required(:name).filled(:string, max_size?: 40)
    end
  end
end
