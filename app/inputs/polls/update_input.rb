# frozen_string_literal: true

class Polls::UpdateInput < ApplicationContract
  json do
    required(:poll).hash do
      optional(:title).filled(:string, max_size?: 40)
      optional(:description).maybe(:string)
      optional(:start_at).filled(Types::JSON::DateTime)
      optional(:end_at).filled(Types::JSON::DateTime)
    end
  end

  rule(poll: :start_at) do
    key.failure('must be in the future') if key? && value <= DateTime.now
  end
end
