# frozen_string_literal: true

class Polls::CreateInput < ApplicationContract
  json do
    required(:poll).hash do
      required(:title).filled(:string, max_size?: 40)
      optional(:description).maybe(:string)
      required(:start_at).filled(Types::JSON::DateTime)
      required(:end_at).filled(Types::JSON::DateTime)
    end
  end
  rule(poll: :start_at) do
    key.failure('must be in the future') if value <= DateTime.now
  end
end
