# frozen_string_literal: true

class API::ConfirmEmailController < API::ApplicationController
  include DI[
    'actions.confirm_email.confirm_by_token'
  ]

  # Confirms email via token
  def update
    authorize! User, to: :confirm_email?

    confirm_by_token.(params[:id])

    respond_with nil
  end
end
