# frozen_string_literal: true

class API::PollsController < API::ApplicationController
  include DI[
    create_poll: 'actions.polls.create',
    update_poll: 'actions.polls.update',
    destroy_poll: 'actions.polls.destroy',
    validate_input: 'services.validate_input'
  ]

  def create
    authorize! Poll, to: :create?

    poll_params = validate_input.(Polls::CreateInput, request.POST)
    poll = create_poll.(poll_params[:poll].merge(user: current_user))

    respond_with poll, serializer: PollSerializer
  end

  def update
    poll = Poll.find(params[:id])
    authorize! poll, to: :update?

    poll_params = validate_input.(Polls::UpdateInput, request.POST)
    update_poll.(poll, poll_params[:poll])

    respond_with poll, serializer: PollSerializer
  end

  def destroy
    poll = Poll.find(params[:id])
    authorize! poll, to: :destroy?
    destroy_poll.(poll)
  end

  def show
    poll = Poll.find(params[:id])
    authorize! poll, to: :show?

    respond_with poll, serializer: PollSerializer
  end
end
