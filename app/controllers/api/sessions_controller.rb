# frozen_string_literal: true

class API::SessionsController < API::ApplicationController
  include DI[
    'actions.auth.sign_in',
    'services.validate_input'
  ]

  def create
    authorize! Session, to: :create?

    session_params = validate_input.(Sessions::CreateInput, request.POST)
    session = sign_in.(**session_params[:session])
    respond_with session,
      include: %i[user],
      serializer: SessionSerializer
  end
end
