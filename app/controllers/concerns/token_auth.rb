# frozen_string_literal: true

module TokenAuth
  TOKEN = 'Authorization'

  private

  def current_user
    current_session&.user
  end

  def current_session
    return @current_session if defined?(@current_session)

    token = parse_header(auth_header)
    @current_session = Session.find_by(id: token)
  end

  def auth_header
    request.headers[TOKEN]
  end

  def parse_header(header)
    return nil unless header =~ /^Bearer (.+)$/

    Regexp.last_match(1).strip
  end
end
