# frozen_string_literal: true

require 'sendgrid-ruby'

class Sendgrid::SendDynamicEmail
  API_KEY = Rails.application.credentials.sendgrid.fetch(:api_key)
  FROM_EMAIL = Rails.application.credentials.sendgrid.fetch(:sender)
  DEFAULT_TEMPLATE_DATA = {
    server_url: Sendgrid::SERVER_URL
  }.freeze

  include SendGrid

  def call(to:, template_id:, template_data:)
    mail = generate_email(to, template_id, template_data)
    api_client.mail._('send').post(request_body: mail.to_json)
  end

  private

  def api_client
    SendGrid::API.new(api_key: API_KEY).client
  end

  # :reek:FeatureEnvy
  def generate_email(to, template_id, template_data)
    Mail.new.tap do |mail|
      mail.from = Email.new(email: FROM_EMAIL)
      mail.template_id = template_id
      mail.add_personalization(personalization(to, template_data))
    end
  end

  def personalization(to, template_data)
    Personalization.new.tap do |personalization|
      personalization.add_to(Email.new(email: to))
      personalization.add_dynamic_template_data(DEFAULT_TEMPLATE_DATA.merge(template_data))
    end
  end
end
