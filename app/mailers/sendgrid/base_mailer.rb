# frozen_string_literal: true

class Sendgrid::BaseMailer
  include DI[
    send_email: 'services.sendgrid.send_dynamic_email'
  ]

  def template_id(template_name)
    Sendgrid::TEMPLATES.fetch(template_name.to_s)
  end
end
