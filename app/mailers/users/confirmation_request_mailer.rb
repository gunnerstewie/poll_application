# frozen_string_literal: true

class Users::ConfirmationRequestMailer < Sendgrid::BaseMailer
  def call(user)
    send_email.(
      to: recipient(user),
      template_id: template_id(:confirmation_request),
      template_data: template_data(user)
    )
  end

  private

  def recipient(user)
    user.email
  end

  def template_data(user)
    {
      token: user.confirmation_token
    }
  end
end
