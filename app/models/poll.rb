# frozen_string_literal: true

class Poll < ApplicationRecord
  belongs_to :user
end
