# frozen_string_literal: true

class Polls::CheckPollTime
  def call(poll)
    return if start_not_later_than_end?(poll)

    raise RequestError.new(I18n.t('errors.polls.invalid_input'), field: 'poll/end_at')
  end

  private

  def start_not_later_than_end?(poll)
    poll.start_at <= poll.end_at
  end
end
