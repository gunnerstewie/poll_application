# frozen_string_literal: true

class AddPollsTable < ActiveRecord::Migration[6.1]
  def change
    create_table :polls do |t|
      t.references :user, null: false, foreign_key: true, index: true
      t.string :title, null: false
      t.string :description
      t.datetime :start_at, null: false
      t.datetime :end_at, null: false
      t.timestamps
    end
  end
end
