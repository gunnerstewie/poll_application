# frozen_string_literal: true

class AddConfirmationAttributesToUser < ActiveRecord::Migration[6.1]
  def change
    change_table :users, bulk: true do |t|
      t.string :confirmation_token
      t.datetime :confirmed_at
    end
    add_index :users, :confirmation_token, unique: true
  end
end
